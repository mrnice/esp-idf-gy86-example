/*
 * GY86 example
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "sdkconfig.h"
#include "i2c.h"
#include "hmc5883l.h"
#include "ms561101ba03.h"
#include "mpu6050.h"
#include "output.h"
#include "common.h"

extern "C" {
	void app_main(void);
}

char buffer[OUTPUT_DATA_SIZE];
QueueHandle_t simple_global_xQueueHandle = NULL;

void app_main(void)
{
    simple_global_xQueueHandle = xQueueCreate(20, sizeof( buffer ));

    xTaskCreate(&task_initI2C, "i2c_init_task", 2048, NULL, 5, NULL);
    vTaskDelay(500/portTICK_PERIOD_MS);
    xTaskCreate(&task_mpu6050, "mpu6050_task", 8192, NULL, 5, NULL);
    xTaskCreate(&task_ms561101ba30, "ms561101ba30_task", 8192, NULL, 5, NULL);
    xTaskCreate(&task_hmc5883l, "hmc5883l_task", 8192, NULL, 5, NULL);
    xTaskCreate(&task_output, "output_task", 8192, NULL, 5, NULL);
}
