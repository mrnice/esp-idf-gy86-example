/*
 * GY86 example
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include <stdio.h>
#include <hmc5883l/hmc5883l.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "hmc5883l.h"
#include "common.h"

void task_hmc5883l(void*){
	char buffer[OUTPUT_DATA_SIZE];

    while (!hmc5883l_init()) {
        printf("Device not found hmc5883l\n");
         vTaskDelay(100/portTICK_PERIOD_MS);
    }

    hmc5883l_set_operating_mode(HMC5883L_MODE_CONTINUOUS);
    hmc5883l_set_samples_averaged(HMC5883L_SAMPLES_8);
    hmc5883l_set_data_rate(HMC5883L_DATA_RATE_07_50);
    hmc5883l_set_gain(HMC5883L_GAIN_1090);

    while (true)
    {
        hmc5883l_data_t data;
        hmc5883l_get_data(&data);

		// FIXME: check result of snprintf!
		snprintf(buffer, OUTPUT_DATA_SIZE, "Magnetic data: X:%.2f mG, Y:%.2f mG, Z:%.2f mG\n", data.x, data.y, data.z);
		xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);

        vTaskDelay(100/portTICK_PERIOD_MS);
    }

	vTaskDelete(NULL);
}
