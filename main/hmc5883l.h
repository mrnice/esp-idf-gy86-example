/*
 * GY86 example
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef HMC5883l_H_
#define HMC5883l_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define SDA_PIN 19
#define SCL_PIN 18

/**
 * task hmc5883l
 */
void task_hmc5883l(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* HMC5883l_H_ */
