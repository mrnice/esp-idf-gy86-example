/*
 * GY86 example
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef MPU6050_H_
#define MPU6050_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * task mpu6050
 */
void task_mpu6050(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* MPU6050_H_ */
