/*
 * GY86 example
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef MS561101BA30_H_
#define MS561101BA30_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * task ms561101ba30
 */
void task_ms561101ba30(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* MS561101BA30_H_ */
