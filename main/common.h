/*
 * GY86 example
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef COMMON_H_
#define COMMON_H_

#define OUTPUT_DATA_SIZE 200

#ifdef __cplusplus
extern "C"
{
#endif

extern QueueHandle_t simple_global_xQueueHandle;

#ifdef __cplusplus
}
#endif

#endif /* COMMON_H_ */
