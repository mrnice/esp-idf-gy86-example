/*
 * GY86 example
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#ifndef I2C_H_
#define I2C_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define SDA_PIN 19
#define SCL_PIN 18

/**
 * task initI2C
 */
void task_initI2C(void *ignore);

#ifdef __cplusplus
}
#endif

#endif /* I2C_H_ */
