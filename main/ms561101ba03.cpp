/*
 * GY86 example
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <ms561101ba03/ms561101ba03.h>

#include "ms561101ba03.h"
#include "common.h"

void task_ms561101ba30(void*){
	char buffer[OUTPUT_DATA_SIZE];

    ms561101ba03_t device =
    {
        .addr = MS561101BA03_ADDR_CSB_LOW,
        .osr  = MS561101BA03_OSR_4096,
    };

    while (!ms561101ba03_init(&device))
        printf("Device not found\n");

    while (true)
    {
        if (!ms561101ba03_get_sensor_data(&device))
            printf("Error reading sensor data from device");

		// FIXME: check result of snprintf!
		snprintf(buffer, OUTPUT_DATA_SIZE, "Temperature in C * 100: %i \nPressure in mbar * 100: %i\n", device.result.temperature, device.result.pressure);
		xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);

        vTaskDelay(100/portTICK_PERIOD_MS);
    }

	vTaskDelete(NULL);
}
