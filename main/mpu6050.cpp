/*
 * Original Copyright
 * Display.c
 *
 *  Created on: 14.08.2017
 *      Author: darek
 *
 * Copyright (C) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 * BSD Licensed as described in the file LICENSE
 */

#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <MPU6050.h>
#include <MPU6050_6Axis_MotionApps20.h>

#include "mpu6050.h"
#include "common.h"

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
uint16_t packetSize = 42;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU

void task_mpu6050(void*){
	char buffer[OUTPUT_DATA_SIZE];

	MPU6050 mpu = MPU6050();
	mpu.initialize();
	mpu.dmpInitialize();

	// This need to be setup individually
	mpu.setXGyroOffset(220);
	mpu.setYGyroOffset(76);
	mpu.setZGyroOffset(-85);
	mpu.setZAccelOffset(1788);

	mpu.setDMPEnabled(true);

	while(1){
		// FIMXE: make the hmc5883l wait for this, for now this works but is dirty as hell
		mpu.setI2CBypassEnabled(true);

	    mpuIntStatus = mpu.getIntStatus();
		// get current FIFO count
		fifoCount = mpu.getFIFOCount();

	    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
	        // reset so we can continue cleanly
	        mpu.resetFIFO();

	    // otherwise, check for DMP data ready interrupt frequently)
	    } else if (mpuIntStatus & 0x02) {
	        // wait for correct available data length, should be a VERY short wait
	        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

	        // read a packet from FIFO

	        mpu.getFIFOBytes(fifoBuffer, packetSize);
	 		mpu.dmpGetQuaternion(&q, fifoBuffer);
			mpu.dmpGetGravity(&gravity, &q);
			mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

			// FIXME: check result of snprintf!
			snprintf(buffer, OUTPUT_DATA_SIZE, "YAW: %3.1f, PITCH: %3.1f, ROLL: %3.1f\n", ypr[0] * 180/M_PI, ypr[1] * 180/M_PI, ypr[2] * 180/M_PI);

			xQueueSend(simple_global_xQueueHandle,&buffer,10/portTICK_PERIOD_MS);
	    }

	    //Best result is to match with DMP refresh rate
	    // Its last value in components/MPU6050/MPU6050_6Axis_MotionApps20.h file line 310
	    // Now its 0x13, which means DMP is refreshed with 10Hz rate
		vTaskDelay(100/portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}
