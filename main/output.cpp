/*
 * GY86 example
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * BSD licensed as described in the file LICENSE
 *
 */

#include <string.h>
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "output.h"
#include "common.h"

void task_output(void *p)
{
    char buffer[OUTPUT_DATA_SIZE];
    
    while (true)
    {
        if(xQueueReceive(simple_global_xQueueHandle, &buffer[0], 10/portTICK_PERIOD_MS))
        {
            printf("%s", buffer);
        }        
        vTaskDelay(1/portTICK_PERIOD_MS);
    }
}
