# Example of using GY86 module with ESP32-IDF drivers

This is an example project for how to use the GY86 module
with the ESP32 ESP-IDF.

Export your IDF_PATH and toolchain path as usual e.g.

```sh
$ export $IDF_PATH=$HOME/path/to/esp-idf
$ export PATH="$PATH:$HOME/path/to/xtensa-esp32-elf/bin"
```

Clone this repository with:

```sh
$ git clone --recursive https://gitlab.com/mrnice/esp-idf-gy86-example.git
```

Configure the sdkconfig with:

```sh
$ make menuconfig
```

Then you can use

```sh
$ make flash
```
To flash the example to your ESP32.

The example output should be something like:

```sh
$ socat /dev/ttyUSB0,b115200 STDOUT
YAW: 52.6, PITCH: -6.6, ROLL: 1.3
Magnetic data: X:184.00 mG, Y:-156.40 mG, Z:-509.68 mG
Temperature in C * 100: 2479 
Pressure in mbar * 100: 96420

...
```
